defmodule RoutingManager do
    use GenServer
    def start_link(x) do
        state = Enum.map 0..31, fn x -> {0,0}
        Enum.map 0..15, fn x -> {0,0} end end
        GenServer.start_link(__MODULE__,state,name: Peer.via_tuple(x))
        
    end

    def init(state) do
        {:ok, state}
      end

    def prefix_matcher(len,id,list,i,list2) do
        id1 = elem(id,1)
        node_slice = String.slice(id1,0..len-1)
        node_slice_next = String.slice(id1,0..len)
        item = Enum.at(list,i)
        item1 = elem(item, 1)
        item_slice = String.slice(item1,0..len-1)
        item_slice_next = String.slice(item1,0..len)
        if(item_slice==node_slice && !(item_slice_next==node_slice_next)) do
            list2 = [item|list2]
        
        end
        if i+1<length list do
         
            prefix_matcher(len,id,list,i+1,list2)
            
        else
            list3 = list2 
            
        end
    end

    def empty?([]), do: true
    def empty?(list) when is_list(list) do
      false
    end
  

    def create_routing_table(node_id,grid,i,list,routing_pid,key) do
        
        id = elem(node_id,1)
        pid = elem(node_id,0)
        
        neighbors = prefix_matcher(i,node_id,list,0,[])
        
        if(empty?(neighbors)) do
            x = 1
        else
            next_digit = String.at(id,i+1)
            Enum.each neighbors, fn n -> 
                x = elem(n,1)
                :global.sync
                grid = get_routing_table(routing_pid)
               #IO.inspect grid
                if(String.at(x,i) == "A") do
                    pos = 10
                else if(String.at(x,i) == "B") do
                    pos = 11
                else if(String.at(x,i) == "C") do
                    pos = 12
                else if(String.at(x,i) == "D") do
                    pos = 13
                else if(String.at(x,i) == "E") do
                    pos = 14
                else if(String.at(x,i) == "F") do
                    pos = 15
                else {pos,_} = Integer.parse(String.at(x,i))
                end
                end
                end
                end
                end 
                end
                
                     replaced = List.replace_at(Enum.at(grid,i),pos,n) 
                     grid = List.replace_at(grid,i,replaced)
                     
                                     
            end
            
        end

        
        
        if(i+1<= String.length id) do
           
            create_routing_table(node_id,grid,i+1,list,routing_pid,key)
        
       
    end
    GenServer.cast(routing_pid,{:grid,grid})
end

    def routing_algo(routing_pid,key) do
        #grid = get_routing_table(routing_pid)
        Peer.receive_request(routing_pid,key)
    end


    def handle_cast({:grid,grid}, st) do
        st = grid
        
        {:noreply, grid}
      end
      def handle_call({:get_grid},_from,st) do
       
        
        {:reply,st,st}
      end
      def get_routing_table(routing_pid) do
        :global.sync()
        
        GenServer.call(routing_pid, {:get_grid})
      end

end
