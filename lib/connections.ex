defmodule Connections do
    def find_ip_address(i) do
        list = Enum.at(:inet.getif() |> Tuple.to_list,1)
        ip = ""
        if elem(Enum.at(list,i),0) == {127, 0, 0, 1} do
            find_ip_address(i+1) 
        else
         ip = elem(Enum.at(list,i),0) |> Tuple.to_list |> Enum.join(".")
        end
    end
    def connections do
        ip=find_ip_address(0)
        :global.sync()
        {_, serverPid} = Node.start(:"serverBoss@#{ip}")
        cookie = Application.get_env(self(), :cookie)
        Node.set_cookie(cookie)
        GenServer.start_link(__MODULE__,[],name: Server) 
    end
    def init(data) do
        {:ok,data}
      end
    
end