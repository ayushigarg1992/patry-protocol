defmodule Tracker do
    use GenServer
    def start_link(map) do
        GenServer.start_link(__MODULE__,[],name: Track)
    end
    
    def init(map) do

        {:ok, map}
     
    end
    
    def get_list(pid) do
        :global.sync()
        GenServer.call(pid, :get_pids)
    end
    def handle_call(:get_pids, _from, list) do
        
        {:reply, list, list}
    end
    def handle_cast({:add_pid,item,hash}, map) do
        map = [{item,hash}|map]
        map = map |> List.keysort(1)
        #map = Enum.sort(map)
        {:noreply, map}
    end
    def add_item(item,pid) do
    
    GenServer.cast(pid,{:add_pid,item})
    get_list(pid)
    end

end