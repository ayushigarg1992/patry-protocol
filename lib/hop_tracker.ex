defmodule HopTracker do
    use GenServer
    def start_link do
        GenServer.start_link(__MODULE__,0,name: HopTrack)
    end
    
    def init(count) do
        
        {:ok, count}
     
    end
    
    def get_hops(pid) do
        :global.sync()
        GenServer.call(pid, :get_hops)
    end
    def handle_call(:get_hops, _from, count) do
        :global.sync
        {:reply, count, count}
    end
    def handle_cast({:set_count,hops}, state) do
        state = hops
        
        {:noreply,state}
    end
    

end