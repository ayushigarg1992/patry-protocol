defmodule Peer do
    use GenServer
    def start_link(pid1,x,hash,key,hop_pid,list_pid) do
        {ok, item} = GenServer.start_link(__MODULE__,0 ,name: via_tuple(x))
        GenServer.cast(pid1,{:add_pid,item,hash})
        list = Tracker.get_list(pid1)
        {_, router} = RoutingManager.start_link(item)
        grid = Enum.map 0..31, fn x -> {0,0}

        Enum.map 0..15, fn x -> {0,0} end end
        RoutingManager.create_routing_table({item,hash},grid,0,list,router,key)
        
        
        receive_request(item,key,pid1,hop_pid,router,list_pid)
            
        

        
        
    end
    
    def init(hops) do
        
        {:ok,hops}
    end
    def listen() do
        receive do 
            
            {pid,key,pid1,hop_pid,router,list_pid}->receive_request(pid,key,pid1,hop_pid,router,list_pid) 
        end
        #listen()
    end
    def receive_request(pid,key,pid_tracker,hop_pid,router,list_pid) do
        grid = RoutingManager.get_routing_table(router)
        hop_count = HopTracker.get_hops(hop_pid)
        list = Tracker.get_list(pid_tracker)
        last_hop = find_closest_leaf(key,list)
        last_hop_pid = elem(last_hop,0)#|>IO.inspect
        if(!(last_hop_pid==pid)) do
            node_idx = Enum.find_index(list,fn x-> elem(x,0)==pid end)
            
            leaf_set = get_leaf_set(node_idx,list,[],node_idx-8)
            if(!Enum.empty?(leaf_set) && Enum.at(leaf_set,0)!=nil && Enum.at(leaf_set,15)!=nil) do
                
                max_len_pref_node = get_max_prefix(list,key,0,"",0)
                {keynum,_} = Integer.parse(key,16)
                
                
                {p,h} = Enum.at(leaf_set,0)
                
                {one,_}= Integer.parse(h,16)#|>IO.inspect
                {p2,h2}=Enum.at(leaf_set,15)
                #IO.puts "_two"
                
                {two,_} = Integer.parse(h2,16)#|>IO.inspect
                #IO.puts "#{one} #{two} are the two numbers to compare for #{inspect self}"
                if(keynum>=one && keynum<=two) do
                    
                    node_to_route = find_closest_leaf(key,leaf_set)
                    pid_to_route = elem(node_to_route,0)
                    lists = Lists.get_list(list_pid)
                    if(!(Enum.member?(lists,pid_to_route)||pid_to_route==0)) do GenServer.cast(list_pid,{:add_pid,pid_to_route}) end
                    
                    if(!(pid_to_route==0 || pid_to_route ==pid||Enum.member?(lists,pid_to_route))) do
                        IO.puts "Current hop #{inspect pid}, next hop #{inspect pid_to_route}"
                        send_message(pid_to_route,key,pid_tracker,hop_pid,router,list_pid)
                    end
                end
            else
                
                tup = Enum.at(list,node_idx)
                len_match = check_pref(elem(tup,1),key,0)
                if (len_match+1<=16) do
                    {ind,_} = Integer.parse(String.at(key,len_match+1),16)
                    node_to_route = Enum.at(Enum.at(grid,len_match+1),ind)

                    pid_to_route = elem(node_to_route,0)
                    lists = Lists.get_list(list_pid)
                    if(!(Enum.member?(lists,pid_to_route)||pid_to_route==0)) do GenServer.cast(list_pid,{:add_pid,pid_to_route}) end
                    #if()
                    
                
                    if(!(pid_to_route==0 || pid_to_route ==pid||Enum.member?(lists,pid_to_route))) do
                       
                        IO.puts "Current hop #{inspect pid}, next hop #{inspect pid_to_route}"
                        send_message(pid_to_route,key,pid_tracker,hop_pid,router,list_pid)
                    
                    else
                    #rare case
                    #IO.puts "not neighbour"
                    node_to_route = Enum.random(list)
                    pid_to_route = elem(node_to_route,0)
                    lists = Lists.get_list(list_pid)
                    if(!(Enum.member?(lists,pid_to_route)||pid_to_route==0)) do GenServer.cast(list_pid,{:add_pid,pid_to_route}) end
                    if(!(pid_to_route==0||pid_to_route==pid||Enum.member?(lists,pid_to_route))) do
                        
                        IO.puts "Current hop #{inspect pid}, next hop #{inspect pid_to_route}"   
                    send_message(pid_to_route,key,pid_tracker,hop_pid,router,list_pid)
                    end
                end
            end
            end
        else
            if (last_hop_pid==pid && hop_count>0) do
                
                IO.puts "Reached destination in #{hop_count} hops"
                #Process.exit(self(),:kill)
                :timer.kill_after(:timer.seconds(0), self())
            end
            
        end
    end
    def send_message(pid_to_route,key,pid1,hop_pid,router,list_pid) do
        count  = HopTracker.get_hops(hop_pid)
        GenServer.cast(hop_pid,{:set_count,count+1})
        GenServer.cast(pid_to_route,{:forward,pid_to_route,key,pid1,hop_pid,router,list_pid})
        #send(pid_to_route,{pid_to_route,key,pid1,hop_pid,router})
    end
    
    def find_closest_leaf(key,set) do
        #IO.puts "CLOSE"
        set_diff = Enum.map(set,fn x-> 
            {res,_} = Integer.parse(elem(x,1),16)
            {num,_} = Integer.parse(key,16)
            result = abs(res-num)
        end)
        
        leaf = Enum.min(set_diff)
        #IO.puts "set_diff = #{inspect set_diff}"
        idx = Enum.find_index(set_diff,fn x->x==leaf end)
        res = Enum.at(set,idx)
    end
    def get_leaf_set(id,list,result,i) do
        result = [Enum.at(list,i)|result]
        if i+1 >id+8 do
            r=result
        else
            get_leaf_set(id,list,result,i+1)
        end
    end
    #get node_id with a maximum matching prefix with the key
    def get_max_prefix(list,key,i,longest_str,max_len) do
        
        tup = Enum.at(list,i)
        hash_string = elem(tup,1)
        temp = max_len
        comp_len = check_pref(hash_string,key,0)
        max_len = max(comp_len,max_len)
        if(max_len>temp) do
            longest_str = hash_string
        end
        if i+1< length list do
        get_max_prefix(list,key,i+1,longest_str,max_len)
        else
            node = tup
        end
     

    end
    #get length of the prefix
    def check_pref(one,two,i) do

        if(String.at(one,i)==String.at(two,i)) do
            check_pref(one,two,i+1)
        else
            len = i
        end

    end
    def via_tuple(node_name) do
        {:via, MyRegistry, {:node_name, node_name}}
    end
    
    def handle_cast({:forward,pid_to_route,key,pid1,hop_pid,router,list_pid}, state) do
        receive_request(pid_to_route,key,pid1,hop_pid,router,list_pid)
        {:noreply, state}
    end

    def handle_call({:get_hops},_from,state) do
         
        {:reply,state,state}
    end
    def get_request_count(node_id) do
        :global.sync()
        GenServer.call(node_id, {:get_state})
    end

    
end