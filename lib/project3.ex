defmodule Project3 do

  def main(args) do
        start_up(args)
    
        receive do
          {:end} -> IO.puts "End"
        end   
      end
  
  def start_up(args) do
    {num_nodes,_} = Integer.parse(Enum.at(args,0))
    {num_reqs,_} = Integer.parse(Enum.at(args,1))
    start(num_nodes,num_reqs)
  end

  def start(nodes,reqs) do
        
        Manager.start_link
        MyRegistry.start_link
        map = Map.new()
        {:ok, pid1} = Tracker.start_link(map)
        {_,hop_pid} = HopTracker.start_link
        r = Enum.random(nodes-50..(nodes))
        key = :crypto.hash(:md5 , "dosProject3#{r}") |> Base.encode16()
        {_, list_pid} = Lists.start_link
        Enum.each 0..nodes, fn x ->
          hash = :crypto.hash(:md5 , "dosProject3#{x}") |> Base.encode16()
          Manager.start_node(pid1,x,hash,key,hop_pid,list_pid)
        end
        

      end
      
      
    
    end